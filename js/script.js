const pizza = {
    pizzaSize: {
        id: '',
        price: null
    },
    pizzaSauce: {
        sauce: '',
        price: null,
        image: '',
    },
    pizzaTopping: {
        topping: '',
        price: '',
        image: '',
    },
    get price() {
        return pizza.pizzaSize.price + pizza.pizzaSauce.price + pizza.pizzaTopping.price
    },
    get sauce() {
        return pizza.pizzaSauce.sauce; 
    },
    get topping() {
        return pizza.pizzaTopping.topping; 
    },
};

document.querySelector("#pizza").addEventListener("click", (e) => {});
const target = document.querySelector('.table'); 
let dragTarget = '';

// Выбираем размер
const [...labelArr] = document.querySelectorAll("label > input");
labelArr.forEach((item) => {
    item.addEventListener("click", (e) => {
        //console.dir(item);
        if (item.checked) {
            const input = item;
            if (input.id == "small") {
                pizza.pizzaSize = {
                    size: "small",
                    price: 30,
                };
            } else if (input.id == "mid") {
                pizza.pizzaSize = {
                    size: "mid",
                    price: 50,
                };
            } else if (input.id == "big") {
                pizza.pizzaSize = {
                    size: "big",
                    price: 70,
                };
            }
            showInfo();
        }
    });
});

function createImg(tag){
    return document.createElement(tag);
}
function removeImg(classImg){
    if(document.querySelector(classImg)){
        document.querySelector(classImg).remove()
        };
}


function showInfo() {
    document.querySelector(".price > p").innerHTML = "Ціна: " + pizza.price; 
    document.querySelector(".sauces > p").innerHTML = "Соуси: " + pizza.sauce;  
    document.querySelector(".topings > p").innerHTML = "Топiнги: " + pizza.topping;
     
    //sauce image
    let sauceImg = createImg('img');
    removeImg('.sauceImg'); 
    sauceImg.setAttribute('src', pizza.pizzaSauce.image);
    sauceImg.classList.add('sauceImg');
    document.querySelector('.table').append(sauceImg);
    //topping image
    let toppingImg = createImg('img');
    removeImg('.toppingImg'); 
    toppingImg.setAttribute('src', pizza.pizzaTopping.image);
    toppingImg.classList.add('toppingImg');
    document.querySelector('.table').append(toppingImg);
}

// Выбираем соус
const [...sauceArr] = document.querySelectorAll('.sauce');
sauceArr.forEach( (item) => {
    item.addEventListener('dragstart', function (event){
        event.dataTransfer.effectAllowed = "move"; 
        const dataDrag = { id: this.id, src: this.src};   
        event.dataTransfer.setData("sauceDrag", JSON.stringify(dataDrag));
        dragTarget = 'sauce';
    });
});

// Выбираем топинги
const [...toppingsArr] = document.querySelectorAll('.topping');
toppingsArr.forEach( (item) => {
    item.addEventListener('dragstart', function (event){
        event.dataTransfer.effectAllowed = "move"; 
        const dataDrag = { id: this.id, src: this.src};   
        event.dataTransfer.setData("toppingsDrag", JSON.stringify(dataDrag));   
        dragTarget = 'topping';
    });  
         
});

    // отменяем стандартное обработчик события dragover.
target.addEventListener('dragover', function (event) {
    if (event.preventDefault) event.preventDefault();
    return false;
});

// прекращаем дальнейшее распространение события по дереву DOM и отменяем возможный стандартный обработчик установленный браузером.
target.addEventListener("drop", function (event) { 
    if (event.preventDefault) event.preventDefault();
    if (event.stopPropagation) event.stopPropagation();

    if(dragTarget == 'sauce'){
        const sauceDrag     = JSON.parse( event.dataTransfer.getData("sauceDrag") ); 
        if (sauceDrag.id == "sauceClassic") {
            pizza.pizzaSauce = {
                sauce: "Кетчуп",
                price: 25,
                image: sauceDrag.src,
            };
        } else if (sauceDrag.id == "sauceBBQ") {
            pizza.pizzaSauce = {
                sauce: "BBQ",
                price: 30,
                image: sauceDrag.src,
            };
        } else if (sauceDrag.id == "sauceRikotta") {
            pizza.pizzaSauce = {
                sauce: "Рiкотта",
                price: 35,
                image: sauceDrag.src,
            };
        } 
        showInfo();
    }else if( dragTarget == 'topping'){
        const toppingDrag   = JSON.parse( event.dataTransfer.getData("toppingsDrag") );
        switch(toppingDrag.id){
            case "moc1": 
                pizza.pizzaTopping = {
                    topping: "Сир звичайний",
                    price: 25,
                    image: toppingDrag.src,
                };
            break;
            case "moc2":
                pizza.pizzaTopping = {
                    topping: "Сир фета",
                    price: 25,
                    image: toppingDrag.src,
                };
            break;
            case "moc3":
                pizza.pizzaTopping = {
                    topping: "Моцарелла",
                    price: 25,
                    image: toppingDrag.src,
                };
            break;
            case "telya":
                pizza.pizzaTopping = {
                    topping: "Телятина",
                    price: 25,
                    image: toppingDrag.src,
                };
            break;
            case "vetch1":
                pizza.pizzaTopping = {
                    topping: "Помiдори",
                    price: 25,
                    image: toppingDrag.src,
                };
            break;
            case "vetch2":
                pizza.pizzaTopping = {
                    topping: "Гриби",
                    price: 25,
                    image: toppingDrag.src,
                };
            break;
        }
        showInfo();
    }

}); 


// FORM
// проверяем input что вводится
function validate( el ){

    function validateEl( reg, val, name){ 

        // Проверка на валидность
        reg.test(val);
        //val.search(reg) !== -1 ? true : errorShow(name);

        //показываем или прячем поле ошибки
        if( val.search(reg) !== -1 ){
            document.querySelector(`.errorInput-${name}`).style.display = 'none';
        }else{
            document.querySelector(`.errorInput-${name}`).style.display = 'block';
        }
    }

    switch( el.id ){
        case 'name':            
            validateEl( /^[А-я]+$/ , el.value, 'name'); 
        break;
        case 'phone':
            //+38(XXX)XXX-XX-XX
            validateEl( /^\+38\([0-9]{3}\)[0-9]{3}-[0-9]{2}-[0-9]{2}$/ , el.value, 'phone');
        break;
        case 'email':
            validateEl( /^[A-z1-9._]+@[a-z1-9._]+.[a-z]{1,4}$/ , el.value, 'email');
        break;
    }

} 

const [...inputArr] = document.querySelectorAll('#contactInfo input');

inputArr.filter( function(el){
    return el.id === 'name' || el.id === 'phone' || el.id === 'email';
}).forEach( function(el){   
    
    // создаем поля ошибок под всеми инпутами
    const   alertM = `Please insert correct ${el.id}`,
    input = document.getElementById(el.id),
    inputOffsetTop = input.offsetTop + 61;
    
    warnWindow = document.createElement('div');
    document.querySelector('#contactInfo').append(warnWindow);
    warnWindow.style.cssText = ` 
        top:${inputOffsetTop}px;
        left:${input.offsetLeft}px; 
    `;
    warnWindow.classList.add(`errorInput-${el.id}`, 'errorInput');
    warnWindow.innerText = alertM;


    el.addEventListener('change', function(){

        validate(this);
        
    });
}); 

//Убегающая кнопка
const   runningButton = document.getElementById('banner');

runningButton.addEventListener('mouseover', function( ){ 
    let randomWidth = Math.floor(Math.random() * 80) + 1 + '%',
        randomHeight = Math.floor(Math.random() * 80) + 1 +'%';
        runningButton.style.bottom = randomWidth;
        runningButton.style.right = randomHeight; 
});

